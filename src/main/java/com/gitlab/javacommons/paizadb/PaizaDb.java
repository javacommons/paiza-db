package com.gitlab.javacommons.paizadb;

import com.j256.ormlite.jdbc.JdbcConnectionSource;

import java.sql.SQLException;

public class PaizaDb {
    public enum DbType {
        DEFAULT(100),
        MEMORY(200),
        MYSQL(300),
        SQLITE(400);
        private final int m_code;

        private DbType(int code) {
            this.m_code = code;
        }

        int code() {
            return this.m_code;
        }
    }

    public static JdbcConnectionSource getJdbcConnectionSource(DbType dbType, String dbName) throws SQLException {
        JdbcConnectionSource connectionSource;
        if (dbType == DbType.MEMORY || dbName.equals(":memory:")) {
            connectionSource = new JdbcConnectionSource("jdbc:sqlite::memory:");
        } else if (dbType == DbType.MYSQL || (dbType == DbType.DEFAULT && System.getenv("ProgramFiles") == null)) {
            connectionSource = new JdbcConnectionSource(
                    "jdbc:mysql://localhost/" + dbName, "root", "");
        } else {
            connectionSource = new JdbcConnectionSource(
                    "jdbc:sqlite:/home/ubuntu/data/" + dbName + ".db3");
        }
        return connectionSource;
    }
}
